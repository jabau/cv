#
# dependencies: sudo apt-get install pandoc context
#

all: html pdf docx rtf

pdf: cv.pdf
cv.pdf: style.tex cv.md
	pandoc --standalone --template style.tex \
	--from markdown --to context \
	-V papersize=A4 \
	-o cv.tex cv.md; \
	context cv.tex

html: cv.html
cv.html: style.css cv.md
	pandoc --standalone -H style.css \
        --from markdown --to html \
        -o cv.html cv.md

docx: cv.docx
cv.docx: cv.md
	pandoc -s cv.md -o cv.docx

rtf: cv.rtf
cv.rtf: cv.md
	pandoc -s cv.md -o cv.rtf

clean:
	rm -f cv.html
	rm -f cv.tex
	rm -f cv.tuc
	rm -f cv.log
	rm -f cv.pdf
	rm -f cv.docx
	rm -f cv.rtf
