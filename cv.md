Jan Bauer Nielsen
=================

---

*Stor interesse for selve programmeringsdisciplinen med de primære sprog Java (SE og EE) og JavaScript.*

*Derudover kendskab til Python, Perl, BASH samt perifert Go, Rust, C, C++, LISP, Standard ML mfl.*

*Udelukkende arbejdet på Linux baserede platforme siden 2002, professionelt såvel som privat.*

*Områder, som har særlig interesse, er databasesystemer, scalability og systemnær programmering.*

*Personlige nøgleord er stabilitet, grundighed og gode samarbejdsevner.*

---

Erhvervserfaring
----------------

2018-
:   *Chefudvikler for backend teams hos DBC as (<http://www.dbc.dk>) med ansvar for at sikre den faglige røde tråd på tværs.*

    * Generel fokus på continuous integration og continuous deployment af services på lokal Kubernetes cluster.
    * Generel fokus på migrations processer (mellem major versioner af Java, applikationsserver, frameworks, m.m.).
    * Generel fokus på udvikling af "best-practice" metodikker.

2011-2018
:   *Programmør ved DBC as (<http://www.dbc.dk>), hvis hovedopgave det er at drive og udvikle den bibliografiske og systemmæssige infrastruktur til bibliotekerne i Danmark.*

    Opgaver i Search team:

    * Viderudvikling af intern metadata- og søgeplatform, fundamentet for den nationale databrønd, som ligger bag eksempelvis portalerne <https://bibliotek.dk/da> og <https://beta.bibliotek.dk/>. (Java SE, Apache Kafka, Apache Solr, Docker)

    Opgaver i I/O team:

    * Lead developer på udvikling af mikroservice baseret platform, som understøtter DBCs og bibliotekernes behov for at kunne følge med i og påvirke processen fra data afleveres eller høstes til de ligger fuldt opdaterede i databrønden. (Java EE, PostgreSQL, REST, JSON, Docker)

2003-2011
:   *Programmør ved Danmarks Tekniske Informationscenter (DTIC) på DTU.*

    Opgaver i udviklingsafdelingen for digitale bibliotekssystemer:

    * Videreudvikling og daglig drift af DTUs artikeldatabase 'DADS'. (Perl, TCL, Z39.50)
    * Primær udvikler af modul for det nationale projekt for statistikindsamling af tidsskriftsforbrug for såvel interne som eksterne ressourcer på de danske forskningsbiblioteker. (Perl, EzProxy)
    * Primær udvikler på det nationale projekt for etablering af web service til direkte dokumentlevering mellem såvel danske som udenlandske forskningsbiblioteker. (Java, Axis2, PostgreSQL)
    * En af hovedkræfterne bag udvikling og design af MetaStore framework til håndtering af databrønd for elektroniske ressourcer såsom tidsskriftsartikler og e-bøger, herunder:
        - Data rens og data konvertering af XML/SGML formater med flere.
        - Udvikling af middleware til samsøgning af et vilkårligt antal datakilder via Z39.50 protokol, inklusiv dynamisk fletning og deduplikering af poster.
        - Indeks generering og optimering for søgemaskinen Zebra (http://indexdata.dk/zebra) med indeks størrelser varierende fra 10.000 til 40.000.000 poster.
        - Udvikling af OAI-PMH grænseflade til metadata eksport.
        - Udvikling af lokal fuldtekst resolver.

2002-2003
:   *Aftjening af civil værnepligt ved Danmarks Tekniske Videncenter (DTV) på DTU i Kgs. Lyngby.*

    Opgaver i afdelingen for Databaser og Systemer:

    * Udvikling af automatiseret brugeropdatering for bibliotekssystemet Aleph. (C++, SQL, PL/SQL)
    * Videreudvikling af Exlibris SFX-server for kontekstsensitiv linkning, herunder især fremstilling af Inter-Library-Loan modul. (Perl)
    * Videreudvikling af Endeavor ScienceServer, herunder især tilpasning af data-loadere. (Perl)

1996-2002

:   *Freelance udviklingsopgaver for Nordsjællands Bilcenter i Hillerød.*

    Herunder:

    * Udvikling af client/server-system til reparations- og lagerkartotek med grafisk frontend. (C++, SQL, ODBC)
    * Udvikling af website. (MS Frontpage, html)

1999
:   *Studentermedarbejder i IT-afdelingen hos Kinnarps Kontormøbler i Nærum.*

Uddannelse
----------

1999-2001
:   *Datamatiker fra Niels Brock Copenhagen Business College med et karaktergennemsnit på 10,5.*

    Valgfagsspecialisering i databasesystemer med eksamensprojekt i objektdatabaser med udgangspunkt i ODMG-standarden version 3.0.
    Afsluttende hovedopgave omhandlende real-time datakommunikation over IP-netværk udarbejdet i samarbejde med firmaet Weibel Scientific,
    som fremstiller instrumentradarer.

1996-1999
:   *Studier ved datalogisk institut på Københavns Universitet med matematik som sidefag.*

    Beståede kurser:

    * DATALOGI 0 (indføring i datalogi og datamatik): skriftlig eksamen.
    * DATALOGI 0 (indføring i datalogi og datamatik): rapporteksamen.
    * DATALOGI 1E (datamatarkitektur og oversætterkonstruktion): skriftlig eksamen.
    * DATALOGI 1E (datamatarkitektur og oversætterkonstruktion): rapporteksamen.
    * DATALOGI 2 (programmeringssprog): skriftlig eksamen.
    * MATEMATIK 1GA (matematisk grundkursus a): skriftlig eksamen.

    Se eventuelt supplerende fagbeskrivelser på <http://www.diku.dk>.

1993-1996
:   *Matematisk student fra Frederiksborg Gymnasium med et karaktergennemsnit på 9,9.*

Supplerende kurser
------------------

2021
:   *"Grundkursus for plejefamilier" hos Socialtilsyn Hovedstaden.*

2017-2014
:   *Deltager på Devoxx UK konferencen (<http://www.devoxx.co.uk/>).*

2014
:   *Java EE - Java Enterprise Edition 7 - Foundation ved Lund & Bendsen Aps.*
    *RESTful API konference ved Lund & Bendsen ApS.*
    *JAX-RS - REST og RESTful Services - Introduction ved Lund & Bendsen ApS.*

2013
:   *Java EE - Java Enterprise Edition 6 - Foundation ved Lund & Bendsen ApS.*
    *GWT - Google Web Toolkit 2 - Foundation ved Lund & Bendsen ApS.*

2010
:   *PostgreSQL DBA/udvikler kursus ved Magnus Hagander fra Redpill Linpro.*

2004
:   *Java, XML og Web Services ved Lund & Bendsen ApS.*

2003
:   *XML Grundkursus (SU0083 1) ved SuperUsers A/S.*
    *XML Validering (SP2459 2b) ved SuperUsers A/S.*
    *XML og Programmering (SP2461 3b) ved SuperUsers A/S.*
    *Udvikling af Web Services (SP2462 4) ved SuperUsers A/S*

----

>  jabau (dot) dk (at) gmail (dot) com • +45 53 60 49 94 • 24-07-1977\
>  Kløverbo 43 - 2665 Vallensbæk Strand, Danmark
